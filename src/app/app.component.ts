import { Component } from '@angular/core';
import { UserService } from "./user/services/user.service";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrl: './app.component.css'
})
export class AppComponent {

  readonly isLogged$ = this.service.getIsLogged$();

  constructor(private readonly service: UserService) {
  }
}
