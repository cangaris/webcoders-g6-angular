export interface ProductPage {
  content: Product[]
  pageable: Pageable
  last: boolean
  totalElements: number
  totalPages: number
  first: boolean
  size: number
  number: number
  sort: Sort
  numberOfElements: number
  empty: boolean
}

export interface Product {
  id: number
  name: string
  content: string
  price: number
  images: Image[]
  tags: Tag[]
  categories: Category[]
}

export interface Image {
  id: number
  uri: string
}

export interface Tag {
  id: number
  name: string
}

export interface Category {
  id: number
  name: string
  content: string
}

export interface Pageable {
  pageNumber: number
  pageSize: number
  sort: Sort
  offset: number
  paged: boolean
  unpaged: boolean
}

export interface Sort {
  empty: boolean
  unsorted: boolean
  sorted: boolean
}
