import { Injectable } from '@angular/core';
import { HttpClient } from "@angular/common/http";
import { Product, ProductPage } from "../models/products";
import { Observable } from "rxjs";
import { Utils } from "../../utils/Utils";

@Injectable({
  providedIn: 'root'
})
export class ProductService {

  private static readonly productUri = 'http://localhost:8080/product';

  constructor(private readonly httpClient: HttpClient) {
  }

  getProducts$(): Observable<ProductPage> {
    return this.httpClient.get<ProductPage>(ProductService.productUri, Utils.httpConfig);
  }

  getProductById$(id: number): Observable<Product> {
    return this.httpClient.get<Product>(`${ProductService.productUri}/${id}`, Utils.httpConfig);
  }
}
