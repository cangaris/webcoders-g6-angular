import { Component } from '@angular/core';
import { ProductService } from "../services/product.service";

@Component({
  selector: 'app-products',
  templateUrl: './products.component.html',
  styleUrl: './products.component.css'
})
export class ProductsComponent {

  readonly productPage$ = this.service.getProducts$();

  constructor(private readonly service: ProductService) {
  }
}

