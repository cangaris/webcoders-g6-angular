import { Component } from '@angular/core';
import { ProductService } from "../services/product.service";
import { ActivatedRoute } from "@angular/router";
import { EMPTY, map, switchMap } from "rxjs";

@Component({
  selector: 'app-product',
  templateUrl: './product.component.html',
  styleUrl: './product.component.css'
})
export class ProductComponent {

  readonly product$ = this.activatedRoute.paramMap
    .pipe(
      map(value => value.has('id') ? value.get('id') : null),
      switchMap(id => id ? this.service.getProductById$(+id) : EMPTY)
    );

  constructor(
    private readonly service: ProductService,
    private readonly activatedRoute: ActivatedRoute
  ) {
  }
}
