import { Component } from '@angular/core';
import { CategoryService } from "../services/category.service";

@Component({
  selector: 'app-categories',
  templateUrl: './categories.component.html',
  styleUrl: './categories.component.css'
})
export class CategoriesComponent {

  readonly categoryPage$ = this.service.getCategories$();

  constructor(private readonly service: CategoryService) {
  }
}
