import { Injectable } from '@angular/core';
import { HttpClient } from "@angular/common/http";
import { Category, CategoryPage } from "../models/categories";
import { Observable } from "rxjs";
import { Utils } from "../../utils/Utils";

@Injectable({
  providedIn: 'root'
})
export class CategoryService {

  private static readonly categoryUri = 'http://localhost:8080/category';

  constructor(private readonly httpClient: HttpClient) {
  }

  getCategories$(): Observable<CategoryPage> {
    return this.httpClient.get<CategoryPage>(CategoryService.categoryUri, Utils.httpConfig);
  }

  getCategoryById$(id: number): Observable<Category> {
    return this.httpClient.get<Category>(`${CategoryService.categoryUri}/${id}`, Utils.httpConfig);
  }
}
