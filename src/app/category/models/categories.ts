export interface CategoryPage {
  content: Category[]
  pageable: Pageable
  last: boolean
  totalElements: number
  totalPages: number
  first: boolean
  size: number
  number: number
  sort: Sort
  numberOfElements: number
  empty: boolean
}

export interface Category {
  id: number
  name: string
  content: string
  images: Image[]
  products: Product[]
}

export interface Image {
  id: number
  uri: string
}

export interface Product {
  id: number
  name: string
  content: string
  price: number
}

export interface Pageable {
  pageNumber: number
  pageSize: number
  sort: Sort
  offset: number
  paged: boolean
  unpaged: boolean
}

export interface Sort {
  empty: boolean
  sorted: boolean
  unsorted: boolean
}
