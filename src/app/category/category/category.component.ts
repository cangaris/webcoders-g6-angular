import { Component } from '@angular/core';
import { CategoryService } from "../services/category.service";
import { ActivatedRoute } from "@angular/router";
import { EMPTY, map, switchMap } from "rxjs";

@Component({
  selector: 'app-category',
  templateUrl: './category.component.html',
  styleUrl: './category.component.css'
})
export class CategoryComponent {

  readonly category$ = this.activatedRoute.paramMap
    .pipe(
      map(value => value.has('id') ? value.get('id') : null),
      switchMap(id => id ? this.service.getCategoryById$(+id) : EMPTY)
    );

  constructor(
    private readonly service: CategoryService,
    private readonly activatedRoute: ActivatedRoute) {
  }
}
