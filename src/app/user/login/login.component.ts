import { Component } from '@angular/core';
import { FormBuilder, Validators } from "@angular/forms";
import { UserService } from "../services/user.service";

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrl: './login.component.css'
})
export class LoginComponent {

  // produkcyjnie formularz nie powinien byc wypełniony!
  readonly form = this.fb.group({
    username: ['pio@now1.pl', [Validators.required, Validators.minLength(4), Validators.maxLength(100)]],
    password: ['Pass11PASS', [Validators.required, Validators.minLength(8), Validators.maxLength(255)]],
  });

  readonly isLogged$ = this.service.getIsLogged$();

  constructor(
    private readonly fb: FormBuilder,
    private readonly service: UserService
  ) {
  }

  onSubmit() {
    if (this.form.valid) {
      const { username, password } = this.form.value;
      this.service.login$(username!, password!)
        .subscribe({
          next: () => alert('poprawnie zalogowano'),
          error: () => alert('login failed'),
        });
    } else {
      alert('Formularz invalid');
    }
  }

  onClick() {
    this.service.logout$()
      .subscribe({
        next: () => alert('poprawnie wylogowano'),
        error: () => alert('logout failed'),
      });
  }
}
