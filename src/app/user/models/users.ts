export interface UserPage {
  content: User[]
  pageable: Pageable
  last: boolean
  totalElements: number
  totalPages: number
  first: boolean
  size: number
  number: number
  sort: Sort
  numberOfElements: number
  empty: boolean
}

export interface User {
  id: number
  firstName: string
  lastName: string
  email: string
  role: string
  nip: string
  phone: Phone
  addresses: Address[]
}

export interface Phone {
  id: number
  prefix: string
  number: string
}

export interface Address {
  id: number
  city: string
  street: string
}

export interface Pageable {
  pageNumber: number
  pageSize: number
  sort: Sort
  offset: number
  paged: boolean
  unpaged: boolean
}

export interface Sort {
  empty: boolean
  unsorted: boolean
  sorted: boolean
}
