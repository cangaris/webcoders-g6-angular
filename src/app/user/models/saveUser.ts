export interface UserSave {
  firstName: string
  lastName: string
  email: string
  nip: string
  pesel: string
  password: string
  role: string
  phone: Phone
  addresses: Address[]
}

export interface Phone {
  number: string
  prefix: string
}

export interface Address {
  city: string
  street: string
}
