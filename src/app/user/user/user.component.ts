import { Component } from '@angular/core';
import { EMPTY, map, switchMap } from "rxjs";
import { ActivatedRoute } from "@angular/router";
import { UserService } from "../services/user.service";

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrl: './user.component.css'
})
export class UserComponent {

  readonly user$ = this.activatedRoute.paramMap
    .pipe(
      map(value => value.has('id') ? value.get('id') : null),
      switchMap(id => id ? this.service.getUserById$(+id) : EMPTY)
    );

  constructor(
    private readonly service: UserService,
    private readonly activatedRoute: ActivatedRoute
  ) {
  }
}
