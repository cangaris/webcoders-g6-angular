import { Component } from '@angular/core';
import { UserService } from "../services/user.service";

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrl: './users.component.css'
})
export class UsersComponent {

  users$ = this.service.getUsers$();

  constructor(private readonly service: UserService) {
  }

  deleteUser(id: number) {
    this.service.deleteUserById$(id)
      .subscribe(() => this.users$ = this.service.getUsers$())
  }
}
