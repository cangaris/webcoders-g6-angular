import { Component } from '@angular/core';
import { FormBuilder } from "@angular/forms";
import { UserService } from "../services/user.service";
import { UserSave } from "../models/saveUser";
import { EMPTY, map, switchMap } from "rxjs";
import { ActivatedRoute } from "@angular/router";

@Component({
  selector: 'app-user-form',
  templateUrl: './user-form.component.html',
  styleUrl: './user-form.component.css'
})
export class UserFormComponent {

  readonly form = this.fb.group({
    firstName: '',
    lastName: '',
    email: '',
    nip: '',
    pesel: '',
    password: '',
    role: '',
    phone: this.fb.group({
      number: '',
      prefix: '',
    }),
    addresses: this.fb.array([
      this.fb.group({
        city: '',
        street: '',
      }),
    ])
  });

  userId: number | null = null; // jezeli id usera istnieje - tryb edycji, nie to tryb dodawania

  constructor(
    private readonly fb: FormBuilder,
    private readonly service: UserService,
    private readonly activatedRoute: ActivatedRoute
  ) {
    this.activatedRoute.paramMap
      .pipe(
        map(value => value.has('id') ? value.get('id') : null),
        switchMap(id => id ? this.service.getUserById$(+id) : EMPTY),
        map(user => {
          this.userId = user.id; // zapamiętanie userId w razie edycji
          // update formularza danymi usera do edycji
          this.form.patchValue({
            firstName: user.firstName,
            lastName: user.lastName,
            email: user.email,
            nip: user.nip,
            pesel: '11111111112', // pesel nie zwracany dla bezpieczenstwa
            password: 'Pass11PASS', // haslo nie jest zwracane dla bezpieczestwa
            role: user.role,
            phone: user.phone,
            addresses: user.addresses,
          })
        })
      ).subscribe();
  }

  insertUser() {
    if (this.form.valid) {
      const userSave = this.form.value as UserSave;
      if (this.userId) {
        // tryb edycji
        this.service.updateUser$(this.userId, userSave)
          .subscribe({
            next: () => alert('success'),
            error: errorResponse => alert(errorResponse.error.join(', ')),
          });
      } else {
        // tryb dodawania danych
        this.service.insertUser$(userSave)
          .subscribe({
            next: () => alert('success'),
            error: errorResponse => alert(errorResponse.error.join(', ')),
          });
      }
    } else {
      alert('form invalid');
    }
  }
}
