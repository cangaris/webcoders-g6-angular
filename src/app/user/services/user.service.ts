import { Injectable } from '@angular/core';
import { HttpClient } from "@angular/common/http";
import { User, UserPage } from "../models/users";
import { BehaviorSubject, Observable, tap } from "rxjs";
import { Utils } from "../../utils/Utils";
import { UserSave } from "../models/saveUser";

@Injectable({
  providedIn: 'root'
})
export class UserService {

  private static readonly userUri = 'http://localhost:8080/user';
  private static readonly accessUri = 'http://localhost:8080';
  private readonly isLogged$$ = new BehaviorSubject(false);

  constructor(private readonly httpClient: HttpClient) {
  }

  getIsLogged$(): Observable<boolean> {
    return this.isLogged$$.asObservable();
  }

  login$(username: string, password: string): Observable<void> {
    const formData = new FormData();
    formData.set('username', username);
    formData.set('password', password);
    return this.httpClient.post<void>(`${UserService.accessUri}/login`, formData, Utils.httpConfig)
      .pipe(tap(() => this.isLogged$$.next(true)))
  }

  logout$(): Observable<void> {
    return this.httpClient.post<void>(`${UserService.accessUri}/logout`, null, Utils.httpConfig)
      .pipe(tap(() => this.isLogged$$.next(false)))
  }

  getUsers$(): Observable<UserPage> {
    return this.httpClient.get<UserPage>(UserService.userUri, Utils.httpConfig);
  }

  getUserById$(id: number): Observable<User> {
    return this.httpClient.get<User>(`${UserService.userUri}/${id}`, Utils.httpConfig);
  }

  insertUser$(userSave: UserSave): Observable<void> {
    return this.httpClient.post<void>(UserService.userUri, userSave, Utils.httpConfig);
  }

  updateUser$(id: number, userSave: UserSave): Observable<void> {
    return this.httpClient.put<void>(`${UserService.userUri}/${id}`, userSave, Utils.httpConfig);
  }

  deleteUserById$(id: number): Observable<void> {
    return this.httpClient.delete<void>(`${UserService.userUri}/${id}`, Utils.httpConfig);
  }
}
