import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LoginComponent } from './login/login.component';
import { ReactiveFormsModule } from "@angular/forms";
import { UsersComponent } from './users/users.component';
import { UserComponent } from './user/user.component';
import { RouterLink } from "@angular/router";
import { UserFormComponent } from './user-form/user-form.component';

@NgModule({
  declarations: [
    LoginComponent,
    UsersComponent,
    UserComponent,
    UserFormComponent
  ],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    RouterLink,
  ]
})
export class UserModule {
}
