import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CategoriesComponent } from "./category/categories/categories.component";
import { ProductsComponent } from "./product/products/products.component";
import { CategoryComponent } from "./category/category/category.component";
import { ProductComponent } from "./product/product/product.component";
import { LoginComponent } from "./user/login/login.component";
import { UsersComponent } from "./user/users/users.component";
import { UserComponent } from "./user/user/user.component";
import { UserFormComponent } from "./user/user-form/user-form.component";

const routes: Routes = [
  {
    component: CategoriesComponent,
    path: 'categories'
  },
  {
    component: CategoryComponent,
    path: 'category/:id'
  },
  {
    component: ProductsComponent,
    path: 'products'
  },
  {
    component: ProductComponent,
    path: 'product/:id'
  },
  {
    component: UsersComponent,
    path: 'users'
  },
  {
    component: UserComponent,
    path: 'user/:id'
  },
  {
    component: UserFormComponent,
    path: 'edit-user/:id'
  },
  {
    component: UserFormComponent,
    path: 'add-user'
  },
  {
    component: LoginComponent,
    path: 'login'
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
